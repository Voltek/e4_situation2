// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDj6CpeDr7sqp7pLZU6eenAW0aL9LXsZmI",
    authDomain: "ionice4-8696a.firebaseapp.com",
    databaseURL: "https://ionice4-8696a.firebaseio.com",
    projectId: "ionice4-8696a",
    storageBucket: "ionice4-8696a.appspot.com",
    messagingSenderId: "625473058849",
    appId: "1:625473058849:web:fc21ec059d257856"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
