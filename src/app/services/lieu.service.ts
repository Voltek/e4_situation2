import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference } from '@angular/fire/firestore';
import { map, take } from 'rxjs/operators';
import { Observable } from 'rxjs';

export interface Lieu {
  id?: string,
  nom: string,
  prenom: string,
  rue: string,
  ville: string,
  cp: string,
  tel: string,
  fait: boolean,
  date: Date;
}

@Injectable({
  providedIn: 'root'
})
export class IdeaService {
  private lieux: Observable<Lieu[]>;
  private lieuCollection: AngularFirestoreCollection<Lieu>;

  constructor(private afs: AngularFirestore) {
    this.lieuCollection = this.afs.collection<Lieu>('lieux');
    this.lieux = this.lieuCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return { id, ...data };
        });
      })
    );
  }

  getLieuxs(): Observable<Lieu[]> {
    return this.lieux;
  }

  getLieu(id: string): Observable<Lieu> {
    return this.lieuCollection.doc<Lieu>(id).valueChanges().pipe(
      take(1),
      map(lieu => {
        lieu.id = id;
        return lieu
      })
    );
  }

  addLieu(lieu: Lieu): Promise<DocumentReference> {
    return this.lieuCollection.add(lieu);
  }

  updateLieu(lieu: Lieu): Promise<void> {
    return this.lieuCollection.doc(lieu.id).update({ nom: lieu.nom, prenom: lieu.prenom, rue: lieu.rue, ville: lieu.ville, cp: lieu.cp, tel: lieu.cp, fait: lieu.fait });
  }

  deleteLieu(id: string): Promise<void> {
    return this.lieuCollection.doc(id).delete();
  }
}