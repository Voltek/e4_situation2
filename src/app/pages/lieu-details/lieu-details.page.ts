import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IdeaService, Lieu } from 'src/app/services/lieu.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-idea-details',
  templateUrl: './lieu-details.page.html',
  styleUrls: ['./lieu-details.page.scss'],
})
export class IdeaDetailsPage implements OnInit {

  lieu: Lieu = {
    cp: '',
    nom: '',
    prenom: '',
    rue: '',
    tel: '',
    ville: '',
    fait: false,
    date: new Date ()
  };
  id = null;

  constructor(private activatedRoute: ActivatedRoute, private ideaService: IdeaService,
    private toastCtrl: ToastController, private router: Router) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
  }

  ionViewWillEnter() {
    if (this.id) {
      this.ideaService.getLieu(this.id).subscribe(lieu => {
        this.lieu = lieu;
      });
    }
  }

  addLieu() {
    this.ideaService.addLieu(this.lieu).then(() => {
      this.router.navigateByUrl('/');
      this.showToast('Lieu ajouté');
    }, err => {
      this.showToast('pb ajout :(');
    });
  }

  deleteLieu() {
    this.ideaService.deleteLieu(this.lieu.id).then(() => {
      this.router.navigateByUrl('/');
      this.showToast('Lieu supprimé');
    }, err => {
      this.showToast('pb suppr :(');
    });
  }

  updateLieu() {
    this.ideaService.updateLieu(this.lieu).then(() => {
      this.router.navigateByUrl('/');
      this.showToast('Lieu mis à jour');
    }, err => {
      this.showToast('pb update :(');
    });
  }

  showToast(msg) {
    this.toastCtrl.create({
      message: msg,
      duration: 300
    }).then(toast => toast.present());
  }
}
