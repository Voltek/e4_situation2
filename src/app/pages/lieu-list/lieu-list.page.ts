import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Lieu, IdeaService } from 'src/app/services/lieu.service';

@Component({
  selector: 'app-idea-list',
  templateUrl: './lieu-list.page.html',
  styleUrls: ['./lieu-list.page.scss'],
})
export class IdeaListPage implements OnInit {

  private lieux: Observable<Lieu[]>;

  constructor(private ideaService: IdeaService) { }

  ngOnInit() {
    this.lieux = this.ideaService.getLieuxs();
  }

}
