import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  // { path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: '', loadChildren: './pages/lieu-list/lieu-list.module#IdeaListPageModule' },
  { path: 'lieu', loadChildren: './pages/lieu-details/lieu-details.module#IdeaDetailsPageModule' },
  { path: 'lieu/:id', loadChildren: './pages/lieu-details/lieu-details.module#IdeaDetailsPageModule' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
